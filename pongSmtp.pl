#!/usr/bin/perl

#~ J'ai mis en place un b�b� service pour tester du bon fonctionnement d'un serveur SMTP. En gros vous envoyer un email � ping [arobase] zici [point] fr (en l'occurrence) et vous recevrez en retour un email � pong � avec les ent�tes du message re�u par le serveur.
#~ Le mettre en place � la maison

#~ Pr�-requis : Perl & un serveur smtp configur� (pour moi c'est Postfix)

#~ Ensuite 4 commandes et c'est r�gl� :
#~ Shell
#~ $ mkdir /opt/pongsmtp
#~ $ cp /root/pongSmtp.pl /opt/pongsmtp/pongSmtp.pl # COPIER CE SCRIPT dans /opt/pongsmtp/pongSmtp.pl
#~ $ echo "ping: \"| perl /opt/pongsmtp/pongSmtp.pl\"" >> /etc/aliases
#~ $ newaliases

#~ Note : Si vous mettez en place ce script n'h�sitez pas � m'en faire part. Une petite liste de � ping@dom1.com, ping@dom2.com � peut �tre int�ressante

use strict;
use warnings "all";
use Getopt::Long;
use DBI;

# Version du programme
my $version = '0.2'; my $dateVersion = 'Mars 2013';

# Parametres
my $params = {
    'version'        => $version,
    'dateversion'    => $dateVersion,
    'programme'      => 'pongSmtp',
    'help'           => 0,
    'debug'          => '',
    'from'           => 'ping@zici.fr',
    'subject'        => '[pongSmtp] ',
    'sendmailBin'    => '/usr/sbin/sendmail',
    'sendmailOpt'    => '-t',
    'limitDb'        => '/tmp/pongSmtp.db',
    'limitNb'        => 10,
    'limitTime'      => '-1 hour',
    'limitTimeTxt'   => '1 heures'
};

# Lecture des options ligne de commande
GetOptions(
    'help!'       => \$params->{help},
    'debug:s'     => \$params->{debug},
    'from:s'      => \$params->{from},
    'subject:s'   => \$params->{subject},
    'sendmailBin:s'      => \$params->{sendmailBin},
    'sendmailOpt:s'      => \$params->{sendmailOpt},
    'limitDb:s'   => \$params->{limitDb},
    'limitNb:n'   => \$params->{limitNb},
    'limitTime:s' => \$params->{limitTime},
    'limitTimeTxt:s'     => \$params->{limitTime}
);

if ($params->{help} > 0) {
    print <<TEXTHELP;
Programme : $params->{programme}.pl V$params->{version} - ($params->{dateversion})
    Test un serveur SMTP en envoyant un ping\@hostname.
    Ce script vous r�pondra pong.
    
Perl version : $]

Usage : $params->{programme}.pl [Option ...]

  Option :
    -help                       : Afficher l'aide
    -debug=                     : Log de debug
    -from=                      : Modifier l'�metteur du message de retour
    -subject=                   : Modifier le sujet du message de retour
    -sendmailBin=               : Bin de la commande sendmail (ex : /usr/sbin/sendmail)
    -sendmailOpt=               : Modifier le sujet du message de retour (ex : -t)
    -limitDb=                   : Chemin vers la base SQLite (blanc pour d�sactiver la limite) (D�faut : /tmp/pongSmtp.db)
								  Attention aux permissions sur la destination de la base
    -limitNb=                   : Nombre de tentative autoris� (D�faut : 10)
    -limitTime=                 : D�lais de remise � 0 des tentatives (format sqlite : http://www.sqlite.org/lang_datefunc.html) (D�faut : -1 hour)
    -limitTimeTxt=              : Sera dans l'email pour l'explication (D�faut : 1 hours)

Installation : 
    * Editer votre fichier (exemple : /etc/aliases)
    * Ajouter la ligne :
        ping: "| perl /chemin/pongSmtp.pl --from=ping\@zici.fr -limitNb=35"
    * Lancer la commande "newaliases"

TEXTHELP
    exit();
}


# On r�cup�re les infos du stdin
my $stdin;
my $entetes="";
my $subject="";
my $received="";
while ($stdin = <STDIN>) {
    if ($stdin =~ /^Subject/) {
        # R�cup�ration du sujet
        my ($subjectLabel, $subjectData) = split('Subject: ', $stdin);
        $subject=$subjectData;
	} elsif ($stdin =~ /^Received/) {
		# R�cup�ration du dernier Recevied
		my ($receivedLabel, $receivedBy, $receivedIp) = split(' ', $stdin);
        $received=$receivedIp;
    } elsif ($stdin =~ /^Content-Type/) {
        # On s'arr�te avant le corps du message
        last;
    }
    $entetes=$entetes.$stdin;
}

# Si la limitation est active
my $pongSmtpLogNb=0;
if ($params->{limitDb}) {
    my $db = DBI->connect("dbi:SQLite:".$params->{limitDb}, "", "", {RaiseError => 1, AutoCommit => 1});
    $db->do("CREATE TABLE IF NOT EXISTS pongSmtpLog (id INTEGER PRIMARY KEY, date DATETIME, sender TEXT, received TEXT)");
    $db->do("INSERT INTO pongSmtpLog VALUES (NULL, datetime('now'), '".$ENV{SENDER}."', '".$received."')");
    my $req1 = $db->selectall_arrayref("SELECT id FROM pongSmtpLog WHERE date > datetime('now','".$params->{limitTime}."') AND (sender = '".$ENV{SENDER}."' OR received = '".$received."')");
    $pongSmtpLogNb = scalar(@$req1);
    if ($pongSmtpLogNb > $params->{limitNb}) { 
        exit();
    } else {
        if ($pongSmtpLogNb == $params->{limitNb}) {
            $params->{subject}=$params->{subject}." [LIMIT EXCEEDED] ";
        }
    }
    # On fait le m�nage dans la base
	$db->do("DELETE FROM pongSmtpLog WHERE date < datetime('now','".$params->{limitTime}."')");
}

# Exp�dition du mail de retour (pong)
open(PONG, "|$params->{sendmailBin} $params->{sendmailOpt} -f$params->{from}");
print PONG "To: $ENV{SENDER}\n";
print PONG "From: $params->{from}\n";
print PONG "Subject: $params->{subject}$subject\n\n";
# Si la limitation est active
if ($params->{limitDb}) {
    print PONG "Utilisation du service ".$pongSmtpLogNb."/".$params->{limitNb}." (remise � z�ro toutes les ".$params->{limitTimeTxt}.")\n\n";    
}
print PONG "Voici les ent�tes du message original : \n\n";
print PONG $entetes;
print PONG "\n\npongSmtp projet : http://pongsmtp.zici.fr";
close(PONG);

