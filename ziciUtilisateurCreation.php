<?php

/* 
 * Script de création d'utilisateur du service d'hébergement zici.fr
 * 
 * Exemple de lancement : 
 * 		php ziciUtilisateurCreation.php -u=michel -e=michel@mail1d.com -d=lesitedemichel
 * 
 * Sous licence Beerware
 * Par David Mercereau : http://david.mercereau.info
 */

// A paramètrer :
$CONFIG['emailDomain']='zici.fr';
$CONFIG['webDomain']='zici.fr';
$CONFIG['resellerId']=0;			// Id du revendeur
$CONFIG['templateMaster']=1;		// Template de client utilisé

// Configuration de la connexion ISPconfig
$CONFIG['remoteUser'] = 'ACOMPLETER';
$CONFIG['remotePassword'] = 'CHUUUUUTTTTEEE';
$CONFIG['remoteSoapLocation'] = 'https://localhost:8080/remote/index.php';
$CONFIG['remoteSoapUri'] = 'https://localhost:8080/remote/';

// Dossier de travail :
$CONFIG['dossierTravail'] = '/root/scripts/zici/var';

function toLog($txt, $level) {
	$logfile = $GLOBALS['CONFIG']['dossierTravail'].'/ziciUtilisateurCreation.log';
    file_put_contents($logfile,date("[j/m/y H:i:s]")." - ".$level." - $txt \r\n",FILE_APPEND);
    echo "[$level] $txt\n";
}

// Test arguments
$options = getopt('u:e:d:');
//var_dump($options);
if (count($options) < 3) {
	exit("Certains arguments sont manquants. Il faut le nom, l'email ainsi que le nom de domaine.\n");
}
if (!preg_match('/^[a-z0-9]+$/' , $options['u'])) {
    exit("Le nom d'utilisateur n'est pas valide.\n");
}
if (!filter_var($options['e'], FILTER_VALIDATE_EMAIL)) {
	exit("L'email n'est pas valide.\n");
}
if (!preg_match('/^[a-z0-9.-]+$/' , $options['d'])) {
    exit("Le domaine n'est pas valide.\n");
}

function generer_mot_de_passe($nb_caractere = 9)
{
	$mot_de_passe = "";
	$chaine = "abcdefghjkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ023456789+@!$%?&";
	$longeur_chaine = strlen($chaine);
	for($i = 1; $i <= $nb_caractere; $i++)
	{
		$place_aleatoire = mt_rand(0,($longeur_chaine-1));
		$mot_de_passe .= $chaine[$place_aleatoire];
	}
	return $mot_de_passe;   
}

// Connexion à ISPconfig
$client = new SoapClient(null, array('location' => $CONFIG['remoteSoapLocation'],
	'uri'      => $CONFIG['remoteSoapUri'],
	'stream_context'=> stream_context_create(array('ssl'=> array('verify_peer'=>false,'verify_peer_name'=>false))),
	'trace' => 1));

// Login
if($session_id = $client->login($CONFIG['remoteUser'], $CONFIG['remotePassword'])) {
	toLog('Login Ok. Session ID:'.$session_id, 'info');
}

// Génération du mot de passe
$pwdFile=$CONFIG['dossierTravail'].'/'.$options['u'].'.pwd';
if (file_exists($pwdFile)) {
	$motDePasse=rtrim(file_get_contents($pwdFile));
	toLog('Le mot de passe existe déjà et est '.$motDePasse, 'warn');
} else {
	$motDePasse=generer_mot_de_passe();
	file_put_contents($pwdFile, $motDePasse);
	chmod($pwdFile, 700);
	toLog('Le mot de passe est généré, c\'est '.$motDePasse, 'info');
}

// Générer le nom d'utilisateur valide pour Shell, FTP, BD
// Limite le nombre de caractère sur le nom d'utilisateur (contrainte mysql)
$utilisateurShellFtpBd = preg_replace('#[^A-Za-z0-9]+#', '', $options['d']);
$utilisateurShellFtpBd = trim($utilisateurShellFtpBd, '');
$utilisateurShellFtpBd = strtolower($utilisateurShellFtpBd);
$utilisateurShellFtpBd=mb_strimwidth(rtrim($utilisateurShellFtpBd), 0, 13);

##### Création utilisateur (pour l'accès au panel)
try {
	$client_exist = $client->client_get_by_username($session_id, $options['u']);
	$client_id = $client_exist['client_id'];
	toLog('L\'utilisateur \''.$options['u'].'\' existe déjà ('.$client_id.')', 'warn');
} catch (SoapFault $e) {
	$reseller_id = $CONFIG['resellerId'];
	$params = array(
		'company_name' => '',
		'contact_name' => ''.$options['u'].'',
		'customer_no' => '',
		'vat_id' => '',
		'street' => '',
		'zip' => '',
		'city' => '',
		'state' => '',
		'country' => 'FR',
		'telephone' => '',
		'mobile' => '',
		'fax' => '',
		'email' => ''.$options['e'].'',
		'internet' => '',
		'icq' => '',
		'notes' => '',
		'default_mailserver' => 1,
		'limit_maildomain' => 0,
		'limit_mailbox' => 0,
		'limit_mailalias' => 0,
		'limit_mailaliasdomain' => 0,
		'limit_mailforward' => 0,
		'limit_mailcatchall' => 0,
		'limit_mailrouting' => 0,
		'limit_mailfilter' => 0,
		'limit_fetchmail' => 0,
		'limit_mailquota' => 0,
		'limit_spamfilter_wblist' => 0,
		'limit_spamfilter_user' => 0,
		'limit_spamfilter_policy' => 0,
		'default_webserver' => 0,
		'limit_web_ip' => '',
		'limit_web_domain' => 0,
		'limit_web_quota' => 0,
		'web_php_options' => 'no',
		'limit_web_subdomain' => 0,
		'limit_web_aliasdomain' => 0,
		'limit_ftp_user' => 0,
		'limit_shell_user' => 0,
		'ssh_chroot' => 'no',
		'limit_webdav_user' => 0,
		'default_dnsserver' => 0,
		'limit_dns_zone' => 0,
		'limit_dns_slave_zone' => 0,
		'limit_dns_record' => 0,
		'default_dbserver' => 1,
		'limit_database' => 0,
		'limit_cron' => 0,
		'limit_cron_type' => 'url',
		'limit_cron_frequency' => 55,
		'limit_traffic_quota' => 0,
		'limit_client' => 0,
		'parent_client_id' => 0,
		'username' => ''.$options['u'].'',
		'password' => ''.$motDePasse.'',
		'language' => 'fr',
		'usertheme' => 'default',
		'template_master' => $CONFIG['templateMaster'],
		'template_additional' => '',
		'created_at' => 0
	);
	if ($client_id=$client->client_add($session_id, $reseller_id, $params)) {
		toLog('L\'utilisateur \''.$options['u'].'\' est créé ('.$client_id.')', 'info');
	} else {
		toLog('Echec à la création de l\'utilisateur \''.$options['u'].'\' ', 'error');
		exit();
	}
}

##### Création du site !
// Check s'il existe déjà
$domain_record = $client->sites_web_domain_get($session_id, array('domain' => $options['d'].'.'.$CONFIG['webDomain']));
if (empty($domain_record[0]['domain_id'])) {	
	// Création
	$params = array(
		'server_id' => 1,
		'ip_address' => '*',
		'domain' => $options['d'].'.'.$CONFIG['webDomain'],
		'type' => 'vhost',
		'parent_domain_id' => 0,
		'vhost_type' => 'name',
		'hd_quota' => 300,
		'traffic_quota' => -1,
		'cgi' => 'n',
		'ssi' => 'n',
		'suexec' => 'n',
		'errordocs' => 1,
		'is_subdomainwww' => 1,
		'subdomain' => 'www',
		'php' => 'php-fpm',
		'pm' => 'dynamic',
		'pm_max_children' => 10,
		'pm_start_servers' => 2,
		'php_fpm_use_socket' => 'y',
		'pm_min_spare_servers' => 1,
		'pm_max_spare_servers' => 5,
		'pm_process_idle_timeout' => 10,
		'pm_max_requests' => 0,
		'ruby' => 'n',
		'redirect_type' => '',
		'redirect_path' => '',
		'ssl' => 'n',
		'ssl_state' => '',
		'ssl_locality' => '',
		'ssl_organisation' => '',
		'ssl_organisation_unit' => '',
		'ssl_country' => '',
		'ssl_domain' => '',
		'ssl_request' => '',
		'ssl_key' => '',
		'ssl_cert' => '',
		'ssl_bundle' => '',
		'ssl_action' => '',
		'stats_password' => $motDePasse,
		'stats_type' => 'awstats',
		'allow_override' => 'All',
		'pm_max_requests' => 0,
		'pm_process_idle_timeout' => 10,
		'custom_php_ini' => '',
		'backup_interval' => 'none',
		'backup_copies' => 1,
		'active' => 'y',
		'traffic_quota_lock' => 'n',
		'http_port' => '80',
		'https_port' => '443'
	);
	
	if ($sites_web_id = $client->sites_web_domain_add($session_id, $client_id, $params, $readonly = false)) {
		toLog('Le domaine '.$options['d'].'.'.$CONFIG['webDomain'].' est créé ('.$sites_web_id.')', 'info');
	} else {
		toLog('Echec à la création du domaine '.$options['d'].'.'.$CONFIG['webDomain'], 'error');
		exit();
	}
} else {
	// Existe déjà, récupération de l'ID
	$sites_web_id = $domain_record[0]['domain_id'];
	toLog('Le domaine '.$options['d'].'.'.$CONFIG['webDomain'].' existe déjà ('.$sites_web_id.')', 'warn');
}

##### Base de donnée, utilisateur
$database_record = $client->sites_database_user_get($session_id, array('database_user' => $utilisateurShellFtpBd));
if (empty($database_record[0]['database_user_id'])) {
	try {
		$params = array(
			'server_id' => 1,
			'database_user' => $utilisateurShellFtpBd,
			'database_password' => $motDePasse
		);
		$database_user_id = $client->sites_database_user_add($session_id, $client_id, $params);
		toLog('L\'utilisateur BD '.$utilisateurShellFtpBd.' est créé ('.$database_user_id.')', 'info');
	} catch (SoapFault $e) {
		toLog('Echec à la création de l\'utilisateur BD '.$utilisateurShellFtpBd.' : '.$e, 'error');
	}
} else {
	$database_user_id = $database_record[0]['database_user_id'];
	toLog('L\'utilisateur BD '.$utilisateurShellFtpBd.' existe déjà ('.$database_user_id.')', 'warn');
}

##### Base de donnée
$database_record = $client->sites_database_get($session_id, array('database_name' => $utilisateurShellFtpBd));
if (empty($database_record[0]['database_name'])) {
	try {
		$params = array(
			'server_id' => 1,
			'type' => 'mysql',
			'website_id' => $sites_web_id,
			'database_name' => $utilisateurShellFtpBd,
			'database_user_id' => $database_user_id,
			'database_ro_user_id' => '0',
			'database_charset' => 'UTF8',
			'remote_access' => 'n',
			'remote_ips' => '',
			'backup_interval' => 'none',
			'backup_copies' => 1,
			'active' => 'y'
		);
		$client->sites_database_add($session_id, $client_id, $params);
		toLog('La BD '.$utilisateurShellFtpBd.' est créé', 'info');
	} catch (SoapFault $e) {
		toLog('Echec à la création de la BD '.$utilisateurShellFtpBd.' : '.$e, 'error');
	}
} else {
	toLog('La BD '.$utilisateurShellFtpBd.' existe déjà', 'warn');
}


##### FTP
$ftp_user_record = $client->sites_ftp_user_get($session_id, array('username' => $utilisateurShellFtpBd));
if (empty($ftp_user_record[0]['ftp_user_id'])) {
	try {
		$params = array(
			'server_id' => 1,
			'parent_domain_id' => $sites_web_id,
			'username' => $utilisateurShellFtpBd,
			'password' => $motDePasse,
			'quota_size' => -1,
			'active' => 'y',
			'uid' => 'web'.$sites_web_id,
			'gid' => 'client'.$client_id,
			'dir' => '/var/www/clients/client'.$client_id.'/web'.$sites_web_id,
			'quota_files' => -1,
			'ul_ratio' => -1,
			'dl_ratio' => -1,
			'ul_bandwidth' => -1,
			'dl_bandwidth' => -1
		);
		$ftp_user_id = $client->sites_ftp_user_add($session_id, $client_id, $params);
		toLog('L\'utilisateur FTP '.$utilisateurShellFtpBd.' est créé ('.$ftp_user_id.')', 'info');
	} catch (SoapFault $e) {
		toLog('Echec à la création de l\'utilisateur FTP '.$utilisateurShellFtpBd.' : '.$e, 'error');
	}
} else {
	$ftp_user_id = $ftp_user_record[0]['ftp_user_id'];
	toLog('L\'utilisateur FTP '.$utilisateurShellFtpBd.' existe déjà ('.$ftp_user_id.')', 'warn');
}


##### SHell user
$shell_user_record = $client->sites_shell_user_get($session_id, array('username' => $utilisateurShellFtpBd));
if (empty($shell_user_record[0]['shell_user_id'])) {
	try {
		$params = array(
			'server_id' => 1,
			'parent_domain_id' => $sites_web_id,
			'username' => $utilisateurShellFtpBd,
			'password' => $motDePasse,
			'quota_size' => -1,
			'active' => 'y',
			'puser' => 'web'.$sites_web_id,
			'pgroup' => 'client'.$client_id,
			'shell' => '/bin/bash',
			'dir' => '/var/www/clients/client'.$client_id.'/web'.$sites_web_id,
			'chroot' => 'jailkit'
		);
		$shell_user_id = $client->sites_shell_user_add($session_id, $client_id, $params);
		toLog('L\'utilisateur Shell '.$utilisateurShellFtpBd.' est créé ('.$shell_user_id.')', 'info');
	} catch (SoapFault $e) {
		toLog('Echec à la création de l\'utilisateur Shell '.$utilisateurShellFtpBd.' : '.$e, 'error');
	}
} else {
	$shell_user_id = $shell_user_record[0]['shell_user_id'];
	toLog('L\'utilisateur Shell '.$utilisateurShellFtpBd.' existe déjà ('.$shell_user_id.')', 'warn');
}


##### Email de transport
$mail_forwarding_record = $client->mail_forward_get($session_id, array('source' => $options['d'].'@'.$CONFIG['emailDomain']));
if (empty($mail_forwarding_record[0]['forwarding_id'])) {
	try {
		$params = array(
			'server_id' => 1,
			'source' => $options['d'].'@'.$CONFIG['emailDomain'],
			'destination' => $options['e'],
			'type' => 'forward',
			'active' => 'y'
		);
		$forwarding_id = $client->mail_forward_add($session_id, $client_id, $params);
		toLog('L\'alias mail '.$options['d'].'@'.$CONFIG['emailDomain'].' est créé ('.$forwarding_id.')', 'info');
	} catch (SoapFault $e) {
		toLog('Echec à la création de l\'alias mail '.$options['d'].'@'.$CONFIG['emailDomain'].' : '.$e, 'error');
	}
} else {
	$forwarding_id = $mail_forwarding_record[0]['forwarding_id'];
	toLog('L\'alias mail '.$options['d'].'@'.$CONFIG['emailDomain'].' existe déjà ('.$forwarding_id.')', 'warn');
}

if($client->logout($session_id)) {
	toLog('Logout', 'info');
}

echo "Appuyer sur ENTRER pour afficher l'email de bienvenu\n";
trim( fgets( STDIN ));



$emailWelcom = "Bonjour ".$options['u'].",

Ton site est prêt : http://".$options['d'].".".$CONFIG['webDomain']."

## Panel d'administration 
Il te permet de gérer tes comptes FTP, base de données ect...
URL : https://panel.zici.fr:8080
Nom d'utilisateur : ".$options['u']."
Mot de passe : $motDePasse

## Accès au fichiers
Les page web sont mettrent dans le répertoire /web)
# Accès FTP
Serveur : ".$options['d'].".".$CONFIG['webDomain']."
Port : 21
Nom d'utilisateur : ".$utilisateurShellFtpBd."
Mot de passe : $motDePasse
Remarque : règler le client FTP en 'mode passif' (pas actif)
# Accès Shell (Sftp)
Serveur : ".$options['d'].".".$CONFIG['webDomain']."
Port : 22
Nom d'utilisateur : ".$utilisateurShellFtpBd."
Mot de passe : $motDePasse

## Base de données
Administration via phpMyAdmin : http://phpmyadmin.zici.fr
Nom de la base : ".$utilisateurShellFtpBd."
Nom d'utilisateur : ".$utilisateurShellFtpBd."
Mot de passe : $motDePasse

## Statistique
Accessible via : http://".$options['d'].".".$CONFIG['webDomain']."/stats/
Nom d'utilisateur : admin
Mot de passe : $motDePasse

## Redirectin mail
Les emails arrivant sur ".$options['d']."@".$CONFIG['emailDomain']." sont redirigés vers ".$options['e']."

";
file_put_contents($CONFIG['dossierTravail'].'/'.$options['u'].'.welcom', $emailWelcom);

echo $emailWelcom;

?>
