#!/bin/bash

# Détection des wordpress sur le serveur
# Ajout des logs du site en question dans fail2ban
# http://david.mercereau.info/wordpress-fail2ban-stopper-la-brute-force-post-wp-login-php/
# Fonctionne avec l'arbo du panel ISPconfig3
# A mettre en tâche planifié

fail2banConf='/etc/fail2ban/jail.d/apache-wp-login.conf'

echo -n "[apache-wp-login]
enabled = true
port    = http,https
filter  = apache-wp-login
maxretry = 8
logpath = " > $fail2banConf

find /var/www/clients -name wp-login.php | while IFS=$'\n' read f ; do
    clientId=`echo $f | cut -d"/" -f5`
    siteId=`echo $f | cut -d"/" -f6`
    echo "	/var/www/clients/$clientId/$siteId/log/access.log " >> $fail2banConf
done

/etc/init.d/fail2ban restart >/dev/null
