# e-mail co2 ispconfig

Un petit script qui envoi un e-mail à chaque boîte e-mail installé sur un serveur ISPconfig pour faire le point sur l'impact environnemental du stockage de ces e-mails

## Installer

Dépendance : Installer "phpmailer"

```bash
composer require phpmailer/phpmailer
```

Configurer l'entête du script email-co2-ispconfig.php, toutes les variables "$CONFIG[*]"

Lancer avec : 

```bash
php  email-co2-ispconfig.php 
```

Note : vous pouvez activer le mode debug pour faire un test dans le fichier  email-co2-ispconfig.php : 

```php
// Debug : true/false
$CONFIG['debug'] = true;
// Debug : en mode debug 
$CONFIG['debugOnlyEmail'] = 'votreemail@domaine.fr'; // Seul cette boîte recevra l'email
```

## Méthodologie de calcul

* 1Mo (1000000o) de mail stocké = 19g de co2 annuel
* Equivalent 100g co2 : 
  * 2 jours d’éclairage avec 1 ampoule à incandescence
  * 1km parcourus avec une voiture essence d’étiquette B
  * La fabrication de 100 feuilles de papier de 80g

Une règle de 3 est appliqué sur cette base de donnée.

Bien sûr tout ceci est une estimation très grossière qui ne tien pas compte de votre cas spécifique mais ça donne un ordre d'idée / ordre de grandeur pour sensibiliser sur le fait qu'un geste aussi banal que cliquer sur envoyer n'est pas si anodin...

Source : 

* https://www.informatiquenews.fr/quelle-empreinte-carbone-pour-mes-emails-61426
* https://www.orange.be/fr/blog/reduire-empreinte-carbone-mail
* https://www.greenit.fr/2009/04/24/combien-de-co2-degage-un-1-kwh-electrique/

## Exemple d'email reçu pour l'impact d'une boîte mail : 

Bonjour,

Je suis David, de Retzo.net, votre hébergeur. Cet e-mail est envoyé  automatiquement pour faire le point sur l'impact environnemental de  votre boîte e-mail xxxxxxxxxxxxxxxxxx.

Actuellement, celle-ci pèse 2.2Go. Le stockage de cette quantité  d'e-mail (sur un serveur allumé 7/7 24/24 pour que vous puissiez y  accéder à tout moment) représente annuellement 44.3Kg de co2 soit : 

- 91 jours d’éclairage avec 1 ampoule à incandescence
- 45 km parcourus avec une voiture essence d’étiquette B
- La fabrication de 4541 feuilles de papier de 80g

Pour réduire cet impact,  je vous encourage à faire le ménage, à  supprimer ou archiver (sur une clé USB par exemple) vos anciens messages (un outil pour vous aider à télécharger/archiver/supprimer vos e-mails : https://lighten-mailbox.zici.fr/). Voici les détails sur l'occupation de votre boîte e-mail : 

- Boîte de réception : 1.7Go (78%)
- Boîte d'envoi / messages envoyés : 394.4Mo (17%)
- Corbeille : 26.3Mo (1%)
- Spam / indésirables  : 750.3Ko (0%)
- Autres dossiers  : 72.7Mo

Quelques astuces à l'avenir : 

- Envoyez vos pièces jointes par un système de lien externe avec date d'expiration : https://drop.picasoft.net/ - https://drop.devloprog.org - https://plik.vulpecula.fr/ - https://dl.retzo.net/ - https://drop.chapril.org/ - https://lufi.ethibox.fr/ - https://drop.underworld.fr - https://drop.infini.fr/ - https://hub.netlib.re/lufi
- Supprimez les messages sans importance
- Désinscrivez-vous des newsletters inutiles
- Évitez de mettre des images en signature 
- Comme pour beaucoup de chose : l'e-mail qui a le moins d'impact, c'est celui qui n'est pas envoyé.

Plus de détails sur la méthodologie de calcul par ici :  https://framagit.org/kepon/vrac/-/blob/master/email-co2-ispconfig/

A votre disposition pour en parler si besoin,

Belle journée, David

P.S. Ce message sera envoyé 1 fois par an, si vous ne souhaitez plus le recevoir, envoyez un e-mail à [xxxxxxl@retzo.net](mailto:xxxxxxxx@retzo.net) en précisant dans le sujet "stop".

## Template d'e-mail reçu pour l'impact d'une boîte mail

Sujet : Impact carbone / énergétique de cette boîte e-mail

Bonjour,

Je suis David, de Retzo.net, votre hébergeur. Cet e-mail est envoyé automatiquement pour faire le point sur l'impact environnemental de votre boîte e-mail [[EMAIL]].

Actuellement, celle-ci pèse [[MAILBOXSIZE]]. Le stockage de cette quantité d'e-mail (sur un serveur allumé 7/7 24/24 pour que vous puissiez y accéder à tout moment) représente annuellement [[MAILBOXSIZECO2]] de co2 soit : [[LISTEEQUICO2]]

Pour réduire cet impact,  je vous encourage à faire le ménage, à supprimer ou archiver (sur une clé USB par exemple) vos anciens messages (un outil pour vous aider à télécharger/archiver/supprimer vos e-mails : https://lighten-mailbox.zici.fr/). Voici les détails sur l'occupation de votre boîte e-mail : [[DETAILFOLDERSIZE]]

Quelques astuces à l'avenir : 

- Envoyez vos pièces jointes par un système de lien externe avec date d'expiration : https://drop.picasoft.net/ - https://drop.devloprog.org - https://plik.vulpecula.fr/ - https://dl.retzo.net/ - https://drop.chapril.org/ - https://lufi.ethibox.fr/ - https://drop.underworld.fr - https://drop.infini.fr/ - https://hub.netlib.re/lufi
- Supprimer les messages sans importance
- Désinscrivez-vous des newsletters inutiles
- Éviter de mettre des images en signature 
- Comme pour beaucoup de chose : l'e-mail qui a le moins d'impact, c'est celui qui n'est pas envoyé.

Plus de détails sur la méthodologie de calcul par ici :  https://framagit.org/kepon/vrac/-/blob/master/email-co2-ispconfig/

A votre disposition pour en parler si besoin,

Belle journée,
David

P.S. Ce message sera envoyé 1 fois par an, si vous ne souhaitez plus le recevoir, envoyez un e-mail à xxxxxxxxx@retzo.net en précisant dans le sujet "stop".

