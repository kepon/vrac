<?php

/* 
 * licence Beerware
 * By David Mercereau : http://david.mercereau.info
 */
  
// ISPconfig Remote user config
$CONFIG['remoteUser'] = 'isp-mail-co2';
$CONFIG['remotePassword'] = '***************';
$CONFIG['remoteSoapLocation'] = 'https://127.0.0.1:8080/remote/index.php';
$CONFIG['remoteSoapUri'] = 'https://127.0.0.1:8080/remote/';
// Email
$CONFIG['logfile'] = '/tmp/email-co2-ispconfig.log';
// Debug : true/false
$CONFIG['debug'] = false; // not send email for true
// Debug : en mode debug 
//~ $CONFIG['debugOnlyEmail'] = 'test@retzo.net'; // only for this mailbox
$CONFIG['debugOnlyEmail'] = false;
// ---------
// Configuration du mailter (mailer)
// ---------
// Mailer host
$CONFIG['mailter']['host']='127.0.0.1';
// Mailer smtpauth (true/false)
$CONFIG['mailter']['smtpauth'] = false;
// Mailer smtpauth username
$CONFIG['mailter']['username'] = '';
// Mailer smtpauth password
$CONFIG['mailter']['password'] = '';
// Mailer smtpauth port
$CONFIG['mailter']['port'] = 25;
// Mailer from email
$CONFIG['mailter']['fromEmail'] = 'vous@votredomaine';
// Mailer from name
$CONFIG['mailter']['fromName'] = 'VOUS - Co2 bilan';
// Maildir config : 
$CONFIG['maildir']['all'] = array('/');
$CONFIG['maildir']['inbox'] = array('/Maildir/cur/');
$CONFIG['maildir']['sent'] = array ('/Maildir/.Sent',
									'/Maildir/.Envoy&AOk-s/');
$CONFIG['maildir']['trash'] = array ('/Maildir/.Trash', 
									'/Maildir/.Corbeille/');
$CONFIG['maildir']['junk'] = array ('/Maildir/.Junk', 
									'/Maildir/.Spam/');
// Maildir : ignorer les boîte sous : (en oct)
$CONFIG['maildir-ignor-size'] = 200000000;
// Ignorer les emails suivant (regex)
$CONFIG['ignoreemail'] = array(
	'@michel.info$',
	'^postmaster'
);
$CONFIG['co2']['g-co2-for-1mo'] = 19;
$CONFIG['co2']['day-bulb-for-1000g-co2'] = 2;
$CONFIG['co2']['km-car-for-1000g-co2'] = 1;
$CONFIG['co2']['paper-for-1000g-co2'] = 100;

// ---------
// Configuration du message
// ---------
$CONFIG['email']['subject'] = "Impact carbone / énergétique de cette boîte e-mail";
$CONFIG['email']['body'] = "<p>Bonjour,</p>
<p>Je suis ****, de ****, votre hébergeur. Cet e-mail est envoyé automatiquement pour faire le point sur l&#39;impact environnemental de votre boîte e-mail [[EMAIL]].</p>
<p>Actuellement, celle-ci pèse [[MAILBOXSIZE]]. Le stockage de cette quantité d&#39;e-mail (sur un serveur allumé 7/7 24/24 pour que vous puissiez y accéder à tout moment) représente annuellement [[MAILBOXSIZECO2]] de co2 soit : [[LISTEEQUICO2]]</p>
<p>Pour réduire cet impact,  je vous encourage à faire le ménage, à supprimer ou archiver (sur une clé USB par exemple) vos anciens messages (un outil pour vous aider à télécharger/archiver/supprimer vos e-mails : <a href='https://lighten-mailbox.zici.fr/' target='_blank' class='url'>https://lighten-mailbox.zici.fr/</a>). Voici les détails sur l&#39;occupation de votre boîte e-mail : [[DETAILFOLDERSIZE]]</p>
<p>Quelques astuces à l&#39;avenir : </p>
<ul>
<li>Envoyez vos pièces jointes par un système de lien externe avec date d&#39;expiration : <a href='https://drop.picasoft.net/' target='_blank' class='url'>https://drop.picasoft.net/</a> - <a href='https://drop.devloprog.org' target='_blank' class='url'>https://drop.devloprog.org</a> - <a href='https://plik.vulpecula.fr/' target='_blank' class='url'>https://plik.vulpecula.fr/</a> - <a href='https://dl.retzo.net/' target='_blank' class='url'>https://dl.retzo.net/</a> - <a href='https://drop.chapril.org/' target='_blank' class='url'>https://drop.chapril.org/</a> - <a href='https://lufi.ethibox.fr/' target='_blank' class='url'>https://lufi.ethibox.fr/</a> - <a href='https://drop.underworld.fr' target='_blank' class='url'>https://drop.underworld.fr</a> - <a href='https://drop.infini.fr/' target='_blank' class='url'>https://drop.infini.fr/</a> - <a href='https://hub.netlib.re/lufi' target='_blank' class='url'>https://hub.netlib.re/lufi</a></li>
<li>Supprimer les messages sans importance</li>
<li>Désinscrivez-vous des newsletters inutiles</li>
<li>Éviter de mettre des images en signature </li>
<li>Comme pour beaucoup de chose : l&#39;e-mail qui a le moins d&#39;impact, c&#39;est celui qui n&#39;est pas envoyé.</li>

</ul>
<p>Plus de détails sur la méthodologie de calcul par ici :  <a href='https://framagit.org/kepon/vrac/-/blob/master/email-co2-ispconfig/' target='_blank' class='url'>https://framagit.org/kepon/vrac/-/blob/master/email-co2-ispconfig/</a></p>
<p>A votre disposition pour en parler si besoin,</p>
<p>Belle journée,
David</p>
<p>P.S. Ce message sera envoyé 1 fois par an, si vous ne souhaitez plus le recevoir, envoyez un e-mail à <a href='mailto:co2-email@retzo.net' target='_blank' class='url'>co2-email@retzo.net</a> en précisant dans le sujet &quot;stop&quot;.</p>";
$CONFIG['email']['altBody'] = "Bonjour,

Je suis ****, de ****, votre hébergeur. Cet e-mail est envoyé automatiquement pour faire le point sur l'impact environnemental de votre boîte e-mail [[EMAIL]].

Actuellement, celle-ci pèse [[MAILBOXSIZE]]. Le stockage de cette quantité d'e-mail (sur un serveur allumé 7/7 24/24 pour que vous puissiez y accéder à tout moment) représente annuellement [[MAILBOXSIZECO2]] de co2 soit : [[LISTEEQUICO2]]

Pour réduire cet impact,  je vous encourage à faire le ménage, à supprimer ou archiver (sur une clé USB par exemple) vos anciens messages (un outil pour vous aider à télécharger/archiver/supprimer vos e-mails : https://lighten-mailbox.zici.fr/). Voici les détails sur l'occupation de votre boîte e-mail : [[DETAILFOLDERSIZE]]

Quelques astuces à l'avenir : 

- Envoyez vos pièces jointes par un système de lien externe avec date d'expiration : https://drop.picasoft.net/ - https://drop.devloprog.org - https://plik.vulpecula.fr/ - https://dl.retzo.net/ - https://drop.chapril.org/ - https://lufi.ethibox.fr/ - https://drop.underworld.fr - https://drop.infini.fr/ - https://hub.netlib.re/lufi
- Supprimez les messages sans importance
- Désinscrivez-vous des newsletters inutiles
- Évitez de mettre des images en signature 
- Comme pour beaucoup de chose : l'e-mail qui a le moins d'impact, c'est celui qui n'est pas envoyé.

Plus de détails sur la méthodologie de calcul par ici :  https://framagit.org/kepon/vrac/-/blob/master/email-co2-ispconfig/

A votre disposition pour en parler si besoin,

Belle journée,
David

P.S. Ce message sera envoyé 1 fois par an, si vous ne souhaitez plus le recevoir, envoyez un e-mail à co2-email@retzo.net en précisant dans le sujet 'stop'.";


// Début du script

//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
//Load Composer's autoloader
require 'vendor/autoload.php';


function convertOctect2humain($value) {
	if ($value > 1000000000) {
		$return=round($value/1024/1024/1024, 1).'Go';
	}elseif ($value > 1000000) {
		$return=round($value/1024/1024, 1).'Mo';
	}elseif ($value > 1000) {
		$return=round($value/1024, 1).'Ko';
	} else {
		$return=$value;
	}
	return $return;
}

function convertGramme2humain($value) {
	if ($value > 1000000) {
		$return=round($value/1024/1024, 1).'Tonne';
	}elseif ($value > 1000) {
		$return=round($value/1024, 1).'Kg';
	} else {
		$return=round($value,1).'g';
	}
	return $return;
}


function TailleDossier($Rep) {
	$Racine=opendir($Rep);
	$Taille=0;
	while($Dossier = readdir($Racine)) {
		if ( $Dossier != '..' And $Dossier !='.' ) {
			//Ajoute la taille du sous dossier
			if(is_dir($Rep.'/'.$Dossier)) $Taille += TailleDossier($Rep.'/'.$Dossier);
			//Ajoute la taille du fichier
			else $Taille += filesize($Rep.'/'.$Dossier);
		}
	}
	closedir($Racine);
	return $Taille;
}

function toLog($txt, $level) {
    file_put_contents($GLOBALS['CONFIG']['logfile'],date("[j/m/y H:i:s]")." - ".$level." - $txt \r\n",FILE_APPEND);
    echo "[$level] $txt\n";
}

function TailleInbox($racine_maildir, $array_maildir) {
	global $CONFIG;
	$maildir_size[$array_maildir] = 0;
	foreach ($CONFIG['maildir'][$array_maildir] as $maildir) {
		if (is_dir($racine_maildir.$maildir)) {
			$maildir_size[$array_maildir] = $maildir_size[$array_maildir] + TailleDossier($racine_maildir.$maildir);
		}
	}
	return $maildir_size[$array_maildir];
}

function search_ignore_config($email) {
	global $CONFIG;
	$return = false;
	foreach ($CONFIG['ignoreemail'] as $ignoreemail) {
		if (preg_match('/'.$ignoreemail.'/', $email)) {
			$return = true;
		}
	}
	return $return;
}

// Connexion to ISPconfig
$client = new SoapClient(null, array('location' => $CONFIG['remoteSoapLocation'],
    'uri'      => $CONFIG['remoteSoapUri'],
    'stream_context'=> stream_context_create(array('ssl'=> array('verify_peer'=>false,'verify_peer_name'=>false))),
    'trace' => 1));
  
// Login
if($session_id = $client->login($CONFIG['remoteUser'], $CONFIG['remotePassword'])) {
    toLog('Login Ok. Session ID:'.$session_id, 'info');
}
  

toLog("Config les boîtes mails sont ignorés si < ".convertOctect2humain($CONFIG['maildir-ignor-size']), 'info');

if ($CONFIG['debug'] == true) {
	toLog("Aucun email ne sera envoyé, le mode debug est à true", 'info');
}

# mail_user_get
try {
	if ($CONFIG['debugOnlyEmail'] != false || $CONFIG['debugOnlyEmail'] != null) {
		$mail_users_get = $client->mail_user_get($session_id, array('email' => $CONFIG['debugOnlyEmail']));
		toLog("Config seul la boîte mail ".$CONFIG['debugOnlyEmail']." est recherché", 'info');
	} else {
		$mail_users_get = $client->mail_user_get($session_id, array());
	}
	
	foreach($mail_users_get as $mail_user_get) {
		toLog("La boîte mail : ".$mail_user_get['email'], 'info');
		$mail_user_get_split=explode('@', $mail_user_get['email']);
		//~ var_dump($mail_user_get);
		$user = $mail_user_get_split[0];
		$domain = $mail_user_get_split[1];
		if (! is_dir($mail_user_get['maildir'])) {
			toLog("[".$mail_user_get['email']."] le répertoire n'existe pas pour ".$mail_user_get['email'], 'warn');
		} else if (search_ignore_config($mail_user_get['email'])) {
			toLog("[".$mail_user_get['email']."] Ignoré car la config l'ignore", 'info');
		} else {
			$maildir_all_size = TailleInbox($mail_user_get['maildir'], 'all');
			$maildir_inbox_size = TailleInbox($mail_user_get['maildir'], 'inbox');
			$maildir_sent_size = TailleInbox($mail_user_get['maildir'], 'sent');
			$maildir_trash_size = TailleInbox($mail_user_get['maildir'], 'trash');
			$maildir_junk_size = TailleInbox($mail_user_get['maildir'], 'junk');
			toLog("[".$mail_user_get['email']."] All : ".convertOctect2humain($maildir_all_size), 'info');
			toLog("[".$mail_user_get['email']."] Inbox : ".convertOctect2humain($maildir_inbox_size), 'info');
			toLog("[".$mail_user_get['email']."] Sent : ".convertOctect2humain($maildir_sent_size), 'info');
			toLog("[".$mail_user_get['email']."] Trash : ".convertOctect2humain($maildir_trash_size), 'info');
			toLog("[".$mail_user_get['email']."] Junk : ".convertOctect2humain($maildir_junk_size), 'info');

			if ($maildir_sent_size == 0 
			|| $maildir_all_size < $CONFIG['maildir-ignor-size']) {
				toLog("[".$mail_user_get['email']."] Ignoré car sous utilisé", 'info');
			} else {
				// Calcul
				$maildir_all_co2=$maildir_all_size*$CONFIG['co2']['g-co2-for-1mo']/1000000;
				toLog("[".$mail_user_get['email']."] All co2 : ".convertGramme2humain($maildir_all_co2), 'info');
				$maildir_all_day_buld=round($maildir_all_co2*$CONFIG['co2']['day-bulb-for-1000g-co2']/1000, 0);
				toLog("[".$mail_user_get['email']."] All day buld : ".$maildir_all_day_buld, 'info');
				$maildir_all_km_car=round($maildir_all_co2*$CONFIG['co2']['km-car-for-1000g-co2']/1000, 0);
				toLog("[".$mail_user_get['email']."] All km car : ".$maildir_all_km_car, 'info');
				$maildir_all_paper=round($maildir_all_co2*$CONFIG['co2']['paper-for-1000g-co2']/1000, 0);
				toLog("[".$mail_user_get['email']."] All paper : ".$maildir_all_paper, 'info');
				
				$LISTEEQUICO2 = "<ul>";
				$LISTEEQUICO2_ALT = "";
				$dataSignificative=0;
				if ($maildir_all_day_buld > 5) {
					$txt = "$maildir_all_day_buld jours d’éclairage avec 1 ampoule à incandescence";
					$LISTEEQUICO2 .= "<li>$txt</li>";
					$LISTEEQUICO2_ALT .= "\n * $txt";
					$dataSignificative++;
				}
				if ($maildir_all_km_car > 5) {
					$txt = "$maildir_all_km_car km parcourus avec une voiture essence d’étiquette B";
					$LISTEEQUICO2 .= "<li>$txt</li>";
					$LISTEEQUICO2_ALT .= "\n * $txt";
					$dataSignificative++;
				}
				if ($maildir_all_paper > 100) {
					$txt = "La fabrication de $maildir_all_paper feuilles de papier de 80g";
					$LISTEEQUICO2 .= "<li>$txt</li>";
					$LISTEEQUICO2_ALT .= "\n * $txt";
					$dataSignificative++;
				}
				$LISTEEQUICO2 .= "</ul>";
				
				$DETAILFOLDERSIZE = "<ul>";
				$DETAILFOLDERSIZE_ALT = "";
				$txt = "Boîte de réception : ".convertOctect2humain($maildir_inbox_size)." (".(round($maildir_inbox_size/$maildir_all_size*100))."%)";
				$DETAILFOLDERSIZE .= "<li>$txt</li>";
				$DETAILFOLDERSIZE_ALT .= "\n * $txt";
				$txt = "Boîte d'envoi / messages envoyés : ".convertOctect2humain($maildir_sent_size)." (".(round($maildir_sent_size/$maildir_all_size*100))."%)";
				$DETAILFOLDERSIZE .= "<li>$txt</li>";
				$DETAILFOLDERSIZE_ALT .= "\n * $txt";
				$txt = "Corbeille : ".convertOctect2humain($maildir_trash_size)." (".(round($maildir_trash_size/$maildir_all_size*100))."%)";
				$DETAILFOLDERSIZE .= "<li>$txt</li>";
				$DETAILFOLDERSIZE_ALT .= "\n * $txt";
				$txt = "Spam / indésirables  : ".convertOctect2humain($maildir_junk_size)." (".(round($maildir_junk_size/$maildir_all_size*100))."%)";
				$DETAILFOLDERSIZE .= "<li>$txt</li>";
				$DETAILFOLDERSIZE_ALT .= "\n * $txt";
				$txt = "Autres dossiers  : ".convertOctect2humain($maildir_all_size-($maildir_inbox_size+$maildir_sent_size+$maildir_trash_size+$maildir_junk_size));
				$DETAILFOLDERSIZE .= "<li>$txt</li>";
				$DETAILFOLDERSIZE_ALT .= "\n * $txt";
				$DETAILFOLDERSIZE .= "</ul>";
				
				// Remplacer les variables
				$email_construction['body'] = $CONFIG['email']['body'];
				$email_construction['altBody'] = $CONFIG['email']['altBody'];
				$email_construction['body']=str_replace('[[EMAIL]]', $mail_user_get['email'], $email_construction['body']);
				$email_construction['altBody']=str_replace('[[EMAIL]]', $mail_user_get['email'], $email_construction['altBody']);
				$email_construction['body']=str_replace('[[LISTEEQUICO2]]', $LISTEEQUICO2, $email_construction['body']);
				$email_construction['altBody']=str_replace('[[LISTEEQUICO2]]', $LISTEEQUICO2_ALT, $email_construction['altBody']);
				$email_construction['body']=str_replace('[[MAILBOXSIZE]]', convertOctect2humain($maildir_all_size), $email_construction['body']);
				$email_construction['altBody']=str_replace('[[MAILBOXSIZE]]', convertOctect2humain($maildir_all_size), $email_construction['altBody']);
				$email_construction['body']=str_replace('[[MAILBOXSIZECO2]]', convertGramme2humain($maildir_all_co2), $email_construction['body']);
				$email_construction['altBody']=str_replace('[[MAILBOXSIZECO2]]', convertGramme2humain($maildir_all_co2), $email_construction['altBody']);
				$email_construction['body']=str_replace('[[DETAILFOLDERSIZE]]', $DETAILFOLDERSIZE, $email_construction['body']);
				$email_construction['altBody']=str_replace('[[DETAILFOLDERSIZE]]', $DETAILFOLDERSIZE_ALT, $email_construction['altBody']);

				if ($CONFIG['debug'] == true) {
					var_dump($email_construction['body']);
					var_dump($email_construction['altBody']);
				}
				
				if ($dataSignificative == 0) {
					toLog("[".$mail_user_get['email']."] Pas assé de données significative pour un envoi", 'info');
				} else if ($CONFIG['debug'] == false) {
					$mail = new PHPMailer(true);
					toLog("[".$mail_user_get['email']."] Envoi de l'email", 'info');
					try {
						//~ $mail->SMTPDebug = SMTP::DEBUG_SERVER;
						$mail->isSMTP();
						$mail->Host       = $CONFIG['mailter']['host'];
						$mail->SMTPAuth   = $CONFIG['mailter']['smtpauth'];
						$mail->Username   = $CONFIG['mailter']['username'];
						$mail->Password   = $CONFIG['mailter']['password'];
						$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
						$mail->Port       = $CONFIG['mailter']['port'];
						$mail->CharSet    = 'UTF-8';
						$mail->Encoding   = '8bit';
						$mail->setFrom($CONFIG['mailter']['fromEmail'], $CONFIG['mailter']['fromName']);
						$mail->addAddress($mail_user_get['email']);
						$mail->isHTML(true);
						$mail->Subject = $CONFIG['email']['subject'];
						$mail->Body    = $email_construction['body'];
						$mail->AltBody = $email_construction['altBody'];
						$mail->send();
						toLog("Message envoyé à ".$mail_user_get['email'], 'info');
						$mail->clearAllRecipients();
					} catch (Exception $e) {
						toLog("Message could not be sent. Mailer Error: {$mail->ErrorInfo}", 'error');
					}
					unset($mail);
					sleep(1);
				}
			}
		}
	}
} catch (SoapFault $e) {
	echo $client->__getLastResponse();
	die('SOAP Error: '.$e->getMessage());
}

  
if($client->logout($session_id)) {
    toLog('Logged out.', 'info');
}
  
?>
