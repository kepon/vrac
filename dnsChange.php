<?php
/* 
 * Script de modification des enregistrements DNS A en cas de changement d'IP serveur
 * 
 * Exemple de lancement : 
 * 		php dnsChange.php -o=IP_ORIGINAL -n=NOUVELLE_IP
 * 
 * Sous licence Beerware
 * Par David Mercereau : http://david.mercereau.info
 */

// Configuration de la connexion ISPconfig
$CONFIG['remoteUser'] = 'UTILISATEUR DISTANT API ISPCONFIG';
$CONFIG['remotePassword'] = 'MOT DE PASSE DE L UTILISATEUR DISTANT API ISPCONFIG';
$CONFIG['remoteSoapLocation'] = 'https://localhost:8080/remote/index.php';
$CONFIG['remoteSoapUri'] = 'https://localhost:8080/remote/';


// Test arguments
$options = getopt('o:n:');
if (count($options) < 2) {
	exit("Certains arguments sont manquants. -o ANCIENNEIP -n NOUVELLEIP .\n");
}
if (!filter_var($options['o'], FILTER_VALIDATE_IP)) {
        exit("L'origin n'est pas une IP valide.\n");
}
if (!filter_var($options['n'], FILTER_VALIDATE_IP)) {
        exit("La nouvelle IP n'est pas une IP valide.\n");
}

// Connexion à ISPconfig
$client = new SoapClient(null, array('location' => $CONFIG['remoteSoapLocation'],
	'uri'      => $CONFIG['remoteSoapUri'],
	'stream_context'=> stream_context_create(array('ssl'=> array('verify_peer'=>false,'verify_peer_name'=>false))),
	'trace' => 1));

// Login
if($session_id = $client->login($CONFIG['remoteUser'], $CONFIG['remotePassword'])) {
	echo "Login Ok. Session ID:".$session_id."\n\n";
}

echo "############################################################################################################################\n";
echo "Recherche de l'IP ".$options['o']." dans tout les enregistrements A des zones DNS du serveur pour la remplacer par ".$options['n']."\n";
echo "############################################################################################################################\n\n";

try {

	$dns_a_gets = $client->dns_a_get($session_id, -1);
        $nb_dns_a_get=0;
	foreach ($dns_a_gets as $dns_a_get) {
                if ($dns_a_get['type'] == 'A' && $dns_a_get['data'] == $options['o']) {
                        $nb_dns_a_get++;
                        $dns_zone_get = $client->dns_zone_get($session_id, $dns_a_get['zone']);
                        echo "Ip trouvé dans la zone ".$dns_zone_get['origin']." avec l'enregistrement ".$dns_a_get['name'].".".$dns_zone_get['origin'].". Voulez-vous la remplacer ? (Y/n) \n";
                        $handle = fopen ("php://stdin","r");
                        $line = fgets($handle);
                        if(trim($line) == 'yes' || trim($line) == 'y' || trim($line) == 'Y' || trim($line) == 'Yes'){
                                $dns_record = $client->dns_a_get($session_id, $dns_a_get['id']);
                                //print_r($dns_record);
                                $dns_record['data']=$options['n'];
                                //print_r($dns_record);
                                $affected_rows = $client->dns_a_update($session_id, $dns_a_get['sys_userid'], $dns_a_get['id'], $dns_record);
                                echo "Le changement à bien été opéré sur ".$affected_rows." enregistrement\n";
                        } else {
                                echo "Aucun changement effectué sur ".$dns_a_get['name'].".".$dns_zone_get['origin']."\n";
                        }
                        fclose($handle);
                }
        }

        if ($nb_dns_a_get == 0) {
                echo "Aucun enregistrement A avec l'IP  ".$options['o']."  n'a été trouvé. \n";
        }
	if($client->logout($session_id)) {
		echo "\nLogged out\n";
	}


} catch (SoapFault $e) {
	echo $client->__getLastResponse();
	die('SOAP Error: '.$e->getMessage()."\n");
}

?>
