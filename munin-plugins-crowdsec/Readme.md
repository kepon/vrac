# Plugin Munin pour Crowdsec

Plugin munin écri en Perl pour monitorer Crowdsec en utilisant sont API local. Actuellement 2 scripts : 
* crowdsec_reasons : Donne les scénarios bloqué localement par Crowdsec
* crowdsec_country : Donne les pays des IP bloqué par Crowdsec

## Installation

Dépendance

```
apt install  libio-all-lwp-perl libjson-perl
```

Copier ces fichiers dans : /etc/munin/plugins

Assurez-vous que le fichier de configuration /etc/munin/plugin-conf.d/crowdsec.conf contient les paramètres corrects :

```
[crowdsec_blocked_countries]
  env.local_api_url http://127.0.0.1:8080/v1
  env.local_api_login your_login
  env.local_api_password your_password
```

Ces identifiants sont récupérable dans /etc/crowdsec/local_api_credentials.yaml

Tester : 
```
# munin-run crowdsec_reasons 
crowdsecurity_http_bf_wordpress_bf_xmlrpc.value 2
crowdsecurity_jira_cve_2021_26086.value 4
crowdsecurity_postfix_spam.value 9
crowdsecurity_http_bad_user_agent.value 31
crowdsecurity_http_probing.value 6
```

Redémarrer ensuite munin-node

```shell
service munin-node restart
```

## Alternative

Plugin similaire : https://github.com/LudovicRousseau/munin-crowdsec/tree/main

## Licence

GPL v3

## Auteur 

David Mercereau : david.mercereau.info/contact/