#!/bin/bash

## IP :
# Chez moi
MOI="A.A.A.A" 
# Mon serveur
SRV1="X.X.X.X"

IPT="/sbin/iptables"
PORTSENTRYCONF="/etc/portsentry/portsentry.conf"

export IPT PORTSENTRYCONF

function portsentryOpen() {
	. ${PORTSENTRYCONF}
	IFS=',' read -ra TCP_PORTS_SPLIT <<< "${TCP_PORTS}"
	for TCP_PORT in "${TCP_PORTS_SPLIT[@]}"; do 
		${IPT} -A INPUT -p tcp --dport ${TCP_PORT} -j ACCEPT
	done
	IFS=',' read -ra UDP_PORTS_SPLIT <<< "${UDP_PORTS}"
	for UDP_PORT in "${UDP_PORTS_SPLIT[@]}"; do 
		${IPT} -A INPUT -p udp --dport ${UDP_PORT} -j ACCEPT
	done
}

# Remise a 0
${IPT} -F
${IPT} -t nat -F

# Les connexions entrantes sont bloquées par défaut
${IPT} -P INPUT DROP
# Les connexions destinées à être routées sont acceptées par défaut
${IPT} -P FORWARD ACCEPT
# Les connexions sortantes sont acceptées par défaut
${IPT} -P OUTPUT ACCEPT


######################
# Règles de filtrage #
######################
# Nous précisons ici des règles spécifiques pour les paquets vérifiant
# certaines conditions.
 
# Pas de filtrage sur l'interface de "loopback"
${IPT} -A INPUT -i lo -j ACCEPT
 
# Accepter le protocole ICMP (notamment le ping)
${IPT} -A INPUT -p icmp -j ACCEPT
  
# Accepter les packets entrants relatifs à des connexions déjà
# établies : cela va plus vite que de devoir réexaminer toutes
# les règles pour chaque paquet.
${IPT} -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT

# ftp 
${IPT} -A INPUT -p tcp --dport 20 -j ACCEPT 
${IPT} -A INPUT -p tcp --dport 21 -j ACCEPT
# Préalabielemnt, pour pure-ftpd : echo "29700 29750" > /etc/pure-ftpd/conf/PassivePortRange ${IPT} -A INPUT -p tcp --dport 29700:29750 -j ACCEPT
# SSH
${IPT} -A INPUT -p tcp --dport 222 -j ACCEPT
# NTP
${IPT} -A INPUT -p udp --dport 123 -j ACCEPT
# smtp
${IPT} -A INPUT -p tcp --dport smtp -j ACCEPT
# Pour test bricolage smtp
${IPT} -A INPUT -p tcp --dport 587 -j ACCEPT
# imap(s)
${IPT} -A INPUT -p tcp --dport 143 -j ACCEPT
${IPT} -A INPUT -p tcp --dport 993 -j ACCEPT
# sieve
${IPT} -A INPUT -p tcp --dport 4190 -j ACCEPT
# dns
${IPT} -A INPUT -p tcp --dport domain -j ACCEPT
${IPT} -A INPUT -p udp --dport domain -j ACCEPT
# http
${IPT} -A INPUT -p tcp --dport http -j ACCEPT
# https
${IPT} -A INPUT -p tcp --dport https -j ACCEPT

# Maintenance 
if [ "$1" == "maintenance" ] ; then
	echo "Maintenance On"
	/usr/sbin/service lighttpd start
	${IPT} -A INPUT -p tcp --dport 81 -j ACCEPT
	${IPT} -t nat -A PREROUTING \! -s ${MOI} -p tcp --dport 80 -j DNAT --to-destination ${SRV1}:81
	${IPT} -t nat -A POSTROUTING -j MASQUERADE
elif [ -f "/var/run/lighttpd.pid" ] ; then
	echo "Maintenance Off"
	/usr/sbin/service lighttpd stop
fi

# Portsentry 
if [ -f ${PORTSENTRYCONF} ] ; then
	portsentryOpen ${IPT} ${PORTSENTRYCONF}
fi

# End
${IPT} -A INPUT -j LOG --log-prefix "iptables denied: "  --log-level 4
${IPT} -A INPUT -j REJECT

# Si vous utilisez fail2ban, relancé à la fin du script :
#/usr/sbin/service fail2ban restart
